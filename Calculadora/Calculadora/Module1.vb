﻿Module Module1

    Sub Main()
        Dim valor1 As Integer
        Dim valor2 As Integer
        Dim total As Integer
        Dim opcion As Char = ""
        While opcion <> "x"
            Console.WriteLine("--- Opciones disponibles ---")
            Console.WriteLine("s -> Sumar")
            Console.WriteLine("r -> Restar")
            Console.WriteLine("m -> Multiplicar")
            Console.WriteLine("d -> Dividir")
            Console.WriteLine("x -> Salir")
            Console.WriteLine("Escoger una opcion segun la letra indicada")
            opcion = Console.ReadKey.KeyChar

            Try
                Select Case opcion
                    Case "s"
                        Console.WriteLine("--- SUMA ---")
                        Console.Write("Ingresar el valor del primer numero: ")
                        valor1 = Console.ReadLine
                        Console.Write("Ingresar el valor del segundo numero: ")
                        valor2 = Console.ReadLine
                        total = valor1 + valor2
                        Console.WriteLine("El valor total es: " & total)
                        Console.WriteLine()
                    Case "r"
                        Console.WriteLine("--- RESTA ---")
                        Console.Write("Ingresar el valor del primer numero: ")
                        valor1 = Console.ReadLine
                        Console.Write("Ingresar el valor del segundo numero: ")
                        valor2 = Console.ReadLine
                        total = valor1 - valor2
                        Console.WriteLine("El valor total es: " & total)
                        Console.WriteLine()
                    Case "m"
                        Console.WriteLine("--- MULTIPLICACION ---")
                        Console.Write("Ingresar el valor del primer numero: ")
                        valor1 = Console.ReadLine
                        Console.Write("Ingresar el valor del segundo numero: ")
                        valor2 = Console.ReadLine
                        total = valor1 * valor2
                        Console.WriteLine("El valor total es: " & total)
                        Console.WriteLine()
                    Case "d"
                        Console.WriteLine("--- DIVISION ---")
                        Console.Write("Ingresar el valor del primer numero: ")
                        valor1 = Console.ReadLine
                        Console.Write("Ingresar el valor del segundo numero: ")
                        valor2 = Console.ReadLine
                        If valor2 <> 0 Then
                            total = valor1 / valor2
                            Console.WriteLine("El valor total es: " & total)
                        Else
                            Console.WriteLine("No se puede realizar dicha operacion")
                        End If
                        Console.WriteLine()
                    Case "x"
                        Console.WriteLine()
                        Console.WriteLine("Gracias por usar la calculadora")
                        Console.ReadKey()
                    Case Else
                        Console.WriteLine()
                        Console.WriteLine("Opcion invalida")
                        Console.WriteLine()
                End Select

            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.WriteLine("Para continuar, presione una tecla.")
                Console.ReadKey()
                Console.WriteLine()
            End Try

        End While


    End Sub

End Module
