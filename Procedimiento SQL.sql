-- DATOS PERSONALES
-- Daniel Alexander de Le�n Mota 201313985
-- Pablo Andr�s Herrera Contreras 201612615

-- CREANDO UNA BASE DE DATOS
CREATE DATABASE tfnos	

-- INDICAR QUE BASE DE DATOS USAR
USE tfnos

-- CREANDO UNA TABLA
CREATE TABLE telefonos(
nombre CHAR(30) NOT NULL,
direccion CHAR(30) NOT NULL,
telefono CHAR(12) PRIMARY KEY NOT NULL,
observaciones CHAR(240))

-- INSERTANDO DATOS EN LA TABLA
INSERT INTO telefonos 
VALUES ('Leticia Aguirre Soriano','Madrid','912345671','Ninguna')

INSERT INTO telefonos 
VALUES ('Pablo Andres Herrera','Guatemala','12345678','Ninguna')

INSERT INTO telefonos 
VALUES ('Daniel Alexander de Leon','Villa Nueva, Guatemala','22446688','Ninguna')

INSERT INTO telefonos 
VALUES ('Prueba','Prueba','15975328','Ninguna')


-- ACTUALIZANDO DATOS EN LA TABLA
UPDATE telefonos 
SET direccion = 'Triana, Sevilla' 
WHERE telefono = '91234567'

UPDATE telefonos 
SET direccion = 'Guatemala, Guatemala' 
WHERE telefono = '12345678'

-- ELIMINANDO DATOS EN LA TABLA
DELETE FROM telefonos
WHERE telefono = '91234567'

DELETE FROM telefonos
WHERE direccion = 'Prueba'


-- ELIMINAR BASE DE DATOS
DROP DATABASE tfnos


-- SELECCIONAR DATOS DE UNA TABLA
SELECT * FROM telefonos

-- SELECCIONAR DATOS DE UNA TABLA EN ORDEN DE UNA COLUMNA
SELECT * FROM telefonos ORDER BY nombre

-- SELECCIONAR DATOS CON CONDICIONES

SELECT * FROM telefonos WHERE telefono < 90000000

-- SELECCIONAR DATOS QUE CONTENGAN UNA PALABRA
SELECT * FROM telefonos WHERE direccion LIKE '%Guatemala%';

-- SELECCIONAR DATOS QUE EMPIECEN CON UNA PALABRA
SELECT * FROM telefonos WHERE direccion LIKE 'Guatemala%';

-- SELECCIONAR DATOS QUE TERMINEN CON UNA PALABRA
SELECT * FROM telefonos WHERE direccion LIKE '%Guatemala';

-- ELIMINAR UNA TABLA DE LA BASE SELECCIONADA
DROP TABLE telefonos