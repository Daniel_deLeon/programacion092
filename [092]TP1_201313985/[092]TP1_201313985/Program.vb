Imports System
Imports System.IO

Module Program
    Sub Main(args As String())
        Dim encabezado As String
        encabezado = "|-----------------------------|" & vbLf &
                     "|Daniel Alexander de Le�n Mota|" & vbLf &
                     "|----------201313985----------|" & vbLf &
                     "|-----------------------------|" & vbLf

        Dim opcion As Char = ""

        While opcion <> "6"
            Try
                Console.WriteLine(encabezado)
                Console.WriteLine("Tarea Preparatoria del Parcial 1:")
                Console.WriteLine("1) Tabla de Multiplicar")
                Console.WriteLine("2) Divisores")
                Console.WriteLine("3) Vocales")
                Console.WriteLine("4) Letra elegida")
                Console.WriteLine("5) Grabar en archivo de texto")
                Console.WriteLine("6) Salir")
                Console.Write("Escojer una opcion, por medio del numero segun lo que necesite realizar:")
                opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case opcion
                    Case "1"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & "Opcion escogida: Tablas de Multiplicar")
                        Dim N1, N2, i As Integer
                        Console.Write("�Cual es el multiplicador de la tabla? ")
                        N1 = Console.ReadLine
                        Console.Write("�Cual es el tama�o de la tabla? ")
                        N2 = Console.ReadLine
                        Console.WriteLine(vbLf & "Mostrando la tabla de multiplicar solicitada")
                        For i = 0 To N2 Step 1
                            Console.WriteLine("--- " & N1 & " X " & i & " -> " & (N1 * i) & " ---")
                        Next
                        Console.WriteLine("El programa se realizo exitosamente, presione para continuar.")
                    Case "2"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & "Opcion escogida: Divisores")
                        Dim N1 As Integer
                        Console.Write("�Cual es el numero por analizar? ")
                        N1 = Console.ReadLine
                        Console.Write("La lista de divisores es: -")
                        For i As Integer = 1 To N1 Step 1
                            If N1 Mod i = 0 Then
                                Console.Write(i & "-")
                            End If
                        Next
                        Console.WriteLine(vbLf & "El programa se realizo exitosamente, presione para continuar.")
                    Case "3"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "Opcion escogida: Vocales")
                        Dim frase As String
                        Console.Write("Ingresar una frase: ")
                        frase = Console.ReadLine
                        Dim contador As Integer = 0
                        Dim letras() As Char = frase.ToCharArray
                        For i As Integer = 0 To (letras.Length - 1) Step 1
                            If letras(i).ToString.ToLower = "a" Or
                               letras(i).ToString.ToLower = "e" Or
                               letras(i).ToString.ToLower = "i" Or
                               letras(i).ToString.ToLower = "o" Or
                               letras(i).ToString.ToLower = "u" Then
                                contador = contador + 1
                            End If
                        Next
                        Console.WriteLine("La cantidad de vocales que se hallan en la frase es de: " & contador)
                        Console.WriteLine(vbLf & "El programa se realizo exitosamente, presione para continuar.")
                    Case "4"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "Opcion escogida: Letra elegida")
                        Dim frase As String
                        Console.Write("Ingresar una frase: ")
                        frase = Console.ReadLine
                        Dim letra As Char
                        Console.Write("Ingresar una letra: ")
                        letra = Console.ReadKey.KeyChar
                        Dim contador As Integer = 0
                        Dim letras() As Char = frase.ToCharArray
                        For i As Integer = 0 To (frase.Length - 1) Step 1
                            If letras(i).ToString.ToLower = letra.ToString.ToLower Then
                                contador = contador + 1
                            End If
                        Next
                        Console.WriteLine(vbLf & "La cantidad de veces que se repite la letra indicada es: " & contador)
                        Console.WriteLine(vbLf & "El programa se realizo exitosamente, presione para continuar.")
                    Case "5"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "Opcion elegida: Grabar archivo de texto")
                        Dim nombre As String = ""
                        While nombre = ""
                            Console.Write("�Cual es el nombre del archivo a guardar? ")
                            nombre = Console.ReadLine
                        End While
                        Dim mensaje As String
                        Console.Write("�Cual es el contenido del archivo a guardar? ")
                        mensaje = Console.ReadLine

                        Dim objStreamWriter As StreamWriter
                        objStreamWriter = New StreamWriter("..\..\..\" & nombre & ".txt")
                        objStreamWriter.WriteLine(mensaje)
                        objStreamWriter.Close()

                        Console.WriteLine("El archivo fue almacenado exitosamente, en la carpeta del programa.")
                        Console.WriteLine(vbLf & "El programa se realizo exitosamente, presione para continuar.")
                    Case "6"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & "Gracias por utilizar nuestro programa, presione cualquier tecla para salir.")
                    Case Else
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & "ERROR: La opcion escogida no es valida, para regresar presione cualquier tecla.")
                End Select
            Catch ex As Exception
                Console.WriteLine("El error fue debido a: " & ex.Message)
                Console.WriteLine("Para regresar presione cualquier tecla:")
            End Try
            Console.ReadKey()
            Console.Clear()
        End While
    End Sub
End Module
